<?php
/*
Template Name: LFTW 2017 Speakers
*/
get_header('food-tech-week-speakers-2017');

wp_enqueue_script(
    'scrolltofixed-js',
    get_stylesheet_directory_uri() . '/bower_components/scrolltofixed/jquery-scrolltofixed.js'
);

wp_enqueue_script(
    'scrollto_jquery_js',
    get_stylesheet_directory_uri() . '/assets/components/jquery.scrollTo/jquery.scrollTo.min.js'
);

wp_enqueue_script(
    'main-js',
    get_stylesheet_directory_uri() . '/js/food-tech-week-2017.js',
    [],
    generate_file_version('/js/food-tech-week-2017.js')
);

wp_enqueue_script(
    'selectticketfactory',
    get_stylesheet_directory_uri() . '/js/selectticketfactory.js',
    [],
    generate_file_version('/js/selectticketfactory.js')
);

$dataToBePassed = array(
    'home' => get_site_url(),
    'logged_in' => is_user_logged_in(),
    'source' => null,
    'event_id' => 155
);

if(isset($_GET['source'])){
    $dataToBePassed['source'] = $_GET['source'];
}

if(is_user_logged_in()){
    $user = wp_get_current_user();
    $contact = new ManageContact();
    $contact->init_contact($user->ID);

    $dataToBePassed['contact_id'] = $contact->get_contact_id();
    $dataToBePassed['title'] = $contact->get_job_title();
    $dataToBePassed['phone'] = $contact->get_phone_number();
    $dataToBePassed['company_name'] = $contact->get_company_name();
    $dataToBePassed['first_name'] = $contact->get_first_name();
    $dataToBePassed['last_name'] = $contact->get_last_name();
    $dataToBePassed['email'] = $contact->get_contact_email();
    $dataToBePassed['contact_type'] = $contact->get_type();
    $dataToBePassed['is_startup'] = $contact->get_company_status();
}

wp_localize_script( 'main-js', 'site', $dataToBePassed );

?>

    <div id="lftw-2017" role="main">
        <?php do_action( 'foundationpress_before_content' ); ?>

        <!-- INFO BAR STARTS  -->
        <div id="sticky-anchor"></div>


        <!-- SPEAKERS STARTS  -->
        <div class="row speakers">
            <div class="hr-sect">
                <h1>2017 LONDON FOOD TECH WEEK SPEAKERS &AMP; INVESTORS</h1>
            </div>
            <!-- HEADLINE SPEAKER INVESTORS MODERATORS STARTS -->
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">
                <br>
                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-1">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06225520/kimberly-hurd.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Alex Petrides</b>
                        <i class="speaker-position"><br>Allplants</i>
                    </a>
                </div> -->
                <div class="column speaker-column">
                    <a href="#speak-modal-2">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/16174734/Andrew-Stephen.jpeg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Andrew Stephen</b>
                        <i class="speaker-position"><br>The Sustainable Restaurant Association</i>
                        <i class="speaker-position"><br>CEO</i>
                    </a>
                </div>
                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-3">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Angelo Satto</b>
                        <i class="speaker-position"><br>TBC</i>
                        <i class="speaker-position"><br>TBC</i>
                    </a>
                </div> -->
                <div class="column speaker-column">
                    <a href="#speak-modal-4">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10162503/anthony-rowland.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Anthony Rowland</b>
                        <i class="speaker-position"><br>Bridgepoint</i>
                        <i class="speaker-position"><br></i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-5">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153142/Antoine-Nussenbaum.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Antoine Nussenbaum</b>
                        <i class="speaker-position"><br>Felix Capital</i>
                        <i class="speaker-position"><br>Partner & Co-founder</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-6">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10163813/ashton-crosby.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Ashton Crosby</b>
                        <i class="speaker-position"><br>TRUE</i>
                        <i class="speaker-position"><br>Investment Director</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-7">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02161515/SORTEDfood.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Barry, Ben, Jamie, Mike</b>
                        <i class="speaker-position"><br>SORTEDfood</i>
                        <i class="speaker-position"><br>Co-Founders</i>
                    </a>
                </div> -->
                <div class="column speaker-column">
                    <a href="#speak-modal-8">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10165418/carolina-riotto.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Carolina Riotto</b>
                        <i class="speaker-position"><br>Unilever</i>
                        <i class="speaker-position"><br>Senior Marketing Manager</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-60">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/03/02045531/Chris-Fung.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Chris Fung</b>
                        <i class="speaker-position"><br>Crussh Fit Food &amp; Juice Bars</i>
                        <i class="speaker-position"><br>CEO</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-9">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10164937/chris-holmes.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Chris Holmes</b>
                        <i class="speaker-position"><br>Shift</i>
                        <i class="speaker-position"><br>Managing Director</i>
                    </a>
                </div>
                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-10">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/16173128/Ivan-Schofield.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Ivan Schofield</b>
                        <i class="speaker-position"><br>Itsu</i>
                        <i class="speaker-position"><br>CEO</i>
                    </a>
                </div> -->

                <div class="column speaker-column">
                    <a href="#speak-modal-11">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/09/christopher-persson.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Chris Persson</b>
                        <i class="speaker-position"><br>RECAPEX</i>
                        <i class="speaker-position"><br>General Partner</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-12">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10165538/christopher-moore.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Christopher Moore</b>
                        <i class="speaker-position"><br>The Clink</i>
                        <i class="speaker-position"><br>Chief Executive</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-13">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153144/Christopher-Slim.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Christopher Slim</b>
                        <i class="speaker-position"><br>Nicoya Invest</i>
                        <i class="speaker-position"><br>Partner & Food Tech Investor</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-14">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/08/Daniel_Vennard-1.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Daniel Vennard</b>
                        <i class="speaker-position"><br>World Resources Institute</i>
                        <i class="speaker-position"><br>Global Programme Director</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-15">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/09/21152120/darren-goldsby.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Darren Goldsby</b>
                        <i class="speaker-position"><br>The Jamie Oliver Group</i>
                        <i class="speaker-position"><br>Chief Digital Officer</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-16">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153147/David-McIntyre-Airbnb.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">David McIntyre</b>
                        <i class="speaker-position"><br>Airbnb</i>
                        <i class="speaker-position"><br>Global Head of Food</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-17">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205821/denise-obsajsnik.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Denise Obsajsnik</b>
                        <i class="speaker-position"><br>My Chef's Table</i>
                        <i class="speaker-position"><br>Founder</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-18">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170233/diane-barlow.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Diane Barlow</b>
                        <i class="speaker-position"><br>Food Standards Agency</i>
                        <i class="speaker-position"><br>Digital Service Manager</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-19">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153151/Dr.-Gary-Stutte.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Dr Gary Stutte</b>
                        <i class="speaker-position"><br>SyNRGE</i>
                        <i class="speaker-position"><br>Scientist & Consultant</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-20">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153150/Dr-Regan-Koch.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Dr Regan Koch</b>
                        <i class="speaker-position"><br>Queen Mary University of London</i>
                        <i class="speaker-position"><br>Lecturer in Human Geography</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-21">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10092610/emma-woods.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Emma Woods</b>
                        <i class="speaker-position"><br>wagamama</i>
                        <i class="speaker-position"><br>Customer Director</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-22">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10093044/fraser-bradshaw.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Fraser Bradshaw</b>
                        <i class="speaker-position"><br>IMBIBA</i>
                        <i class="speaker-position"><br>Partner</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-23">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205743/fredrika-gulfot.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Fredrika Gulfot</b>
                        <i class="speaker-position"><br>Simris</i>
                        <i class="speaker-position"><br>Founder &amp; CEO</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-24">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10093351/gerardo-mazzeo.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Gerardo Mazzeo</b>
                        <i class="speaker-position"><br>Nestle</i>
                        <i class="speaker-position"><br>Global Innovation Director</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-25">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10093655/hannah-seal.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Hannah Seal</b>
                        <i class="speaker-position"><br>Index Ventures</i>
                        <i class="speaker-position"><br>Investor</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-26">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10094323/hulya-erdal.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Hulya Erdal</b>
                        <i class="speaker-position"><br>Made by the Chef</i>
                        <i class="speaker-position"><br>Chef</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-27">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/09/06133537/Jamie-Bolding2-1.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Jamie Bolding</b>
                        <i class="speaker-position"><br>Jungle Creations</i>
                        <i class="speaker-position"><br>Founder & CEO</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-28">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205744/jamie-spafford.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Jamie Spafford</b>
                        <i class="speaker-position"><br>SORTEDfood</i>
                        <i class="speaker-position"><br>Co-Founder</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-29">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10094841/jeremy-knight1.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Jeremy Knight</b>
                        <i class="speaker-position"><br>Hej Coffee</i>
                        <i class="speaker-position"><br>Chief Espresso Officer</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-30">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10095121/joanna-de-koning.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Joanna De Koning</b>
                        <i class="speaker-position"><br>Just Eat</i>
                        <i class="speaker-position"><br>Head of Corporate Communications</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-31">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205829/jonathan-petrides.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Jonathan Petrides (JP)</b>
                        <i class="speaker-position"><br>allplants</i>
                        <i class="speaker-position"><br>Founder</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-32">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Kalinka Kay</b>
                        <i class="speaker-position"><br>TrueStart</i>
                        <i class="speaker-position"><br></i>
                    </a>
                </div> -->

                <div class="column speaker-column">
                    <a href="#speak-modal-33">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/08/Kevin-Duffy_Winnow-1.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Kevin Duffy</b>
                        <i class="speaker-position"><br>Winnow</i>
                        <i class="speaker-position"><br>Co-Founder</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-34">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06225520/kimberly-hurd.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Kimberly Hurd</b>
                        <i class="speaker-position"><br>Tabl</i>
                        <i class="speaker-position"><br>Co-Founder &amp; CEO</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-35">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170535/kirsty-macdonald.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Kirsty Macdonald</b>
                        <i class="speaker-position"><br>Jam Jar Investments</i>
                        <i class="speaker-position"><br>Senior Investment Associate</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-36">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170536/kishan-vasani.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Kishan Vasani</b>
                        <i class="speaker-position"><br>Dishq</i>
                        <i class="speaker-position"><br>Co-Founder &amp; CEO</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-37">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153153/Leonie-Cooper.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Leonie Cooper</b>
                        <i class="speaker-position"><br>London City Hall</i>
                        <i class="speaker-position"><br>Chair of the Environment Committee</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-38">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/09/21152121/libby-andrews.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Libby Andrews</b>
                        <i class="speaker-position"><br>Pho</i>
                        <i class="speaker-position"><br>Head of Marketing</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-39">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170825/lisa-jenkins.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Lisa Jenkins</b>
                        <i class="speaker-position"><br>Caterer</i>
                        <i class="speaker-position"><br>Product Editor</i>
                    </a>
                </div>

                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-40">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170942/mark-palmer.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Mark Palmer</b>
                        <i class="speaker-position"><br>BT</i>
                        <i class="speaker-position"><br></i>
                    </a>
                </div> -->

                <div class="column speaker-column">
                    <a href="#speak-modal-41">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/09/Michael-Atkinson.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Michael Atkinson</b>
                        <i class="speaker-position"><br>Bailiwick Ventures</i>
                        <i class="speaker-position"><br>CEO</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-42">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10090505/nick-popovici.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Nick Popovici</b>
                        <i class="speaker-position"><br>VitaMojo</i>
                        <i class="speaker-position"><br>Co-Founder</i>
                    </a>
                </div>
                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-43">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Oksana Stowe</b>
                        <i class="speaker-position"><br>TrueStart</i>
                        <i class="speaker-position"><br></i>
                    </a>
                </div> -->
                <div class="column speaker-column">
                    <a href="#speak-modal-59">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10174328/oliver-oram.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Oliver Oram</b>
                        <i class="speaker-position"><br>Chainvine</i>
                        <i class="speaker-position"><br>CEO</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-44">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10172621/os-tariq.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Os Tariq</b>
                        <i class="speaker-position"><br>Food Standards Agency</i>
                        <i class="speaker-position"><br></i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-45">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153156/Paul-Newnham.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Paul Newnham</b>
                        <i class="speaker-position"><br>SDG2 Advocacy Hub</i>
                        <i class="speaker-position"><br>Coordinator</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-46">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/09/21152122/peter-martin.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Peter Martin</b>
                        <i class="speaker-position"><br>CGA</i>
                        <i class="speaker-position"><br>Vice President</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-47">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10051349/philip-james.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Philip James</b>
                        <i class="speaker-position"><br>Sheridans</i>
                        <i class="speaker-position"><br>Partner</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-48">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/16173127/Charles-Spence.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Professor Charles Spence</b>
                        <i class="speaker-position"><br>Oxford University</i>
                        <i class="speaker-position"><br>Head of the Crossmodal Research Laboratory at the Department of Experimental Psychology</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-49">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10091749/robin-rowland.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Robin Rowland</b>
                        <i class="speaker-position"><br>Yo! Sushi</i>
                        <i class="speaker-position"><br>CEO</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-50">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Saasha Celestial-One</b>
                        <i class="speaker-position"><br>Olio</i>
                        <i class="speaker-position"><br>Co-Founder</i>
                    </a>
                </div>
                <div class="column speaker-column">
                    <a href="#speak-modal-51">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10171402/sai-sreenivas-kodur.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Sai Sreenivas Kodur</b>
                        <i class="speaker-position"><br>Dishq</i>
                        <i class="speaker-position"><br>Co-Founder &amp; CTO</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-52">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/09/samad-masood.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Samad Masood</b>
                        <i class="speaker-position"><br>House of Genius</i>
                        <i class="speaker-position"><br>London Lead Moderator</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-53">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10171708/steven-novick.png" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Steven Novick</b>
                        <i class="speaker-position"><br>Farmstand</i>
                        <i class="speaker-position"><br>Founder</i>
                    </a>
                </div>

            </div>
            <div class="row speaker-row feat-speakers large-up-4 medium-up-4 small-up-2">

                <div class="column speaker-column">
                    <a href="#speak-modal-54">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10172117/sue-cambage-hughes.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Sue Cambage-Hughes</b>
                        <i class="speaker-position"><br>CGA</i>
                        <i class="speaker-position"><br>Head of Consumer Research</i>
                    </a>
                </div>

                <div class="column speaker-column">
                    <a href="#speak-modal-61">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10175258/trewin-restorick.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Trewin Restorick</b>
                        <i class="speaker-position"><br>Hubbub</i>
                        <i class="speaker-position"><br>CEO</i>
                    </a>
                </div>
                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-55">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Tomoya Yasuda</b>
                        <i class="speaker-position"><br>Cookpad</i>
                        <i class="speaker-position"><br>&nbsp;</i>
                    </a>
                </div> -->
                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-56">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/10/Virginie-Rabant.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Virginie Rabant</b>
                        <i class="speaker-position"><br>Elior</i>
                        <i class="speaker-position"><br>&nbsp;</i>
                    </a>
                </div> -->

                <div class="column speaker-column">
                    <a href="#speak-modal-57">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/16173129/Zoe-Chambers.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Zoe Chambers</b>
                        <i class="speaker-position"><br>Octopus Ventures</i>
                        <i class="speaker-position"><br>Investor</i>
                    </a>
                </div>
                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-58">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Mattia Nanetti</b>
                        <i class="speaker-position"><br>Wenda</i>
                        <i class="speaker-position"><br></i>
                    </a>
                </div> -->

                <!-- <div class="column speaker-column">
                    <a href="#speak-modal-62">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                        <b class="speaker-name">Steven Novick</b>
                        <i class="speaker-position"><br>Farmstand</i>
                        <i class="speaker-position"><br>Founder</i>
                    </a>
                </div> -->
            </div>

            <div>
            <br><br>
            </div>
            <!-- HEADLINE SPEAKER ENDS -->


        </div
        <!-- SPEAKERS ENDS  -->

        <div id="speak-modal-1" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06225520/kimberly-hurd.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Alex Petrides</b>
                        <i class="speaker-position"></i>
                        <i class="speaker-position"><br>Allplants</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    An open discussion about the evolution of taste and how a deep understanding of it defines the food experience.
                    We’ll debate the role and relationship of two major forces influencing society’s taste graph, globalisation and personalisation.

                    <br><br><span class="display-none" id="more-1">We’ll explore the ways in which taste & technology (AI & machine learning) can be used to aid consumer discovery, demonstrated live through audience participation. Co-hosts: Alex Petrides from AllPlants.</span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-1" onclick="this.style.display = 'none';document.getElementById('more-1').style.display = 'block';document.getElementById('less-btn-1').style.display = 'block';">See more</a>
                        <a id="less-btn-1" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-1').style.display = 'none';document.getElementById('more-btn-1').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-2" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/16174734/Andrew-Stephen.jpeg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Andrew Stephen</b>
                        <i class="speaker-position">The Sustainable Restaurant Association</i>
                        <i class="speaker-position"><br>CEO</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Since 2016, Andrew has been the CEO of the Sustainable Restaurant Association (SRA) a not-for-profit organisation focused solely on the challenge of fostering a better food system by directing, nudging, shaping, creating and measuring sustainable change in the food service sector.
                    <br><br><span class="display-none" id="more-2">
They do this through Food Made Good, a collaborative community of chefs, restaurateurs, marketers, foodies and social changers. This community exists on and offline to connect people working together to make food good for people and the planet. Andrew's mission to help us use the power of our appetites wisely.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-2" onclick="this.style.display = 'none';document.getElementById('more-2').style.display = 'block';document.getElementById('less-btn-2').style.display = 'block';">See more</a>
                        <a id="less-btn-2" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-2').style.display = 'none';document.getElementById('more-btn-2').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-3" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Andrew Stephen</b>
                        <i class="speaker-position">The Sustainable Restaurant Association</i>
                        <i class="speaker-position"><br>CEO</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>

                    <br><br><span class="display-none" id="more-3"></span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-3" onclick="this.style.display = 'none';document.getElementById('more-3').style.display = 'block';document.getElementById('less-btn-3').style.display = 'block';">See more</a>
                        <a id="less-btn-3" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-3').style.display = 'none';document.getElementById('more-btn-3').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-4" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10162503/anthony-rowland.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Anthony Rowland</b>
                        <i class="speaker-position">Bridgepoint</i>
                        <i class="speaker-position"><br></i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>

                    <br><br><span class="display-none" id="more-4"></span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-4" onclick="this.style.display = 'none';document.getElementById('more-4').style.display = 'block';document.getElementById('less-btn-4').style.display = 'block';">See more</a>
                        <a id="less-btn-4" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-4').style.display = 'none';document.getElementById('more-btn-4').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-5" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153142/Antoine-Nussenbaum.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Antoine Nussenbaum</b>
                        <i class="speaker-position">Felix Capital</i>
                        <i class="speaker-position"><br>Partner & Co-founder</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Antoine is a Partner and member of the founding team of Felix Capital. He currently sits on the boards of Frichti, Urban Massage, Papier and Rad. Prior to Felix Capital, he worked closely with various early-stage digital startups including Mirakl, Reedsy, 31Dover, Jellynote and helped them launch their businesses.

                    <br><br><span class="display-none" id="more-5">
                        He has been involved since inception with Huckletree, a fast growing co-working operator started by his wife. Prior to moving to London, he was based in Paris and was part of the founding team of NT Valley, a software business.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-5" onclick="this.style.display = 'none';document.getElementById('more-5').style.display = 'block';document.getElementById('less-btn-5').style.display = 'block';">See more</a>
                        <a id="less-btn-5" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-5').style.display = 'none';document.getElementById('more-btn-5').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-6" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10163813/ashton-crosby.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Ashton Crosby</b>
                        <i class="speaker-position">TRUE</i>
                        <i class="speaker-position"><br>Ashton Crosby</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Ashton has 15 years of operating and investing experience in the consumer F&B sector. He is currently Investment Director at True, a mid-market UK PE fund, and sits on the board of online catering company City Pantry.
                    He joined True from Tesco in Business Development team, during which time he was also Commercial Director and COO of the Harris+Hoole coffee chain.
                    <br><br><span class="display-none" id="more-6">
                         Previously Ashton was an investment banker at Nomura in EMEA Consumer/Retail M&A. He holds a Bachelor’s degree from Trinity College (USA) and an MBA from HEC Paris, as well as a Master’s ‘Certificat de Spécialisation’ in luxury goods. Ashton speaks fluent Spanish and is conversational in Chinese (Mandarin), French, and Portuguese
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-6" onclick="this.style.display = 'none';document.getElementById('more-6').style.display = 'block';document.getElementById('less-btn-6').style.display = 'block';">See more</a>
                        <a id="less-btn-6" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-6').style.display = 'none';document.getElementById('more-btn-6').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-7" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02161515/SORTEDfood.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Barry, Ben, Jamie, Mike</b>
                        <i class="speaker-position">SORTEDfood</i>
                        <i class="speaker-position"><br>Co-Founders</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    SORTEDfood is a global movement of friends who share a passion for food, cooking and laughter. Started in 2009, SORTEDfood came to life when a group of old school friends would meet in the pub and share ‘scandalous’ stories of their university life.
                    They started to share basic cooking skills and recipe ideas with each other on the back of beer mats.
                    <br><br><span class="display-none" id="more-7">
                        Following some successful meals and improved diets, they set about sharing their new-found skills with others in similar situations. And so, SORTEDfood was born. 8 years on, there’s now over 2 million people around the world joining this cooking conversation and sharing new recipe ideas. SORTEDfood is set on providing all the practical tips, skills and great recipes needed to inspire you to cook.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-7" onclick="this.style.display = 'none';document.getElementById('more-7').style.display = 'block';document.getElementById('less-btn-7').style.display = 'block';">See more</a>
                        <a id="less-btn-7" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-7').style.display = 'none';document.getElementById('more-btn-7').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-8" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10165418/carolina-riotto.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Carolina Riotto</b>
                        <i class="speaker-position">Unilever</i>
                        <i class="speaker-position"><br>Senior Marketing Manager</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Carolina started her career working for advertising agency then moved to Unilever where she is working for the past 12 years across different categories and brands.
                    <br><br><span class="display-none" id="more-8">
                        During this journey, she has lived in Sao Paulo (home country), Singapore and now London. As a passionate marketer, Carolina is always looking on different ways to connect trends, consumers and brands. Now one of her main passion at work is to land Hellmann’s sustainable living purpose across different countries in the world.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-8" onclick="this.style.display = 'none';document.getElementById('more-8').style.display = 'block';document.getElementById('less-btn-8').style.display = 'block';">See more</a>
                        <a id="less-btn-8" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-8').style.display = 'none';document.getElementById('more-btn-8').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-9" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10164937/chris-holmes.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Chris Holmes</b>
                        <i class="speaker-position">Shift</i>
                        <i class="speaker-position"><br>Managing Director</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Chris heads up Shift’s Healthy Food programme, developing products and services that catalyse changes in existing food categories that lead to better diets.
                    <br><br><span class="display-none" id="more-9">
                        The team are currently focused on fast food with programmes underway in Tower Hamlets, Hackney and Birmingham. This work builds on the success of ‘Box Chicken’, Shift’s development of a healthier fried chicken outlet. Following a 15-year commercial career in the food industry, Chris has spent the last 12-years applying behavioural science to a variety of social issues. His particular interest is in childhood obesity and how to harness the competitive dynamics of consumer markets to deliver pro-social outcomes.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-9" onclick="this.style.display = 'none';document.getElementById('more-9').style.display = 'block';document.getElementById('less-btn-9').style.display = 'block';">See more</a>
                        <a id="less-btn-9" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-9').style.display = 'none';document.getElementById('more-btn-9').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-10" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/16173128/Ivan-Schofield.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Ivan Schofield</b>
                        <i class="speaker-position">Itsu</i>
                        <i class="speaker-position"><br>CEO</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Ivan has been CEO of itsu’s UK retail business since September 2016. From 2002 to 2015 he was MD of KFC in France and (from 2010) Western Europe, growing the KFC brand to £1bn+ of sales and 15,000 employees in 500 restaurants.
                    <br><br><span class="display-none" id="more-10">
                        His earlier career was with Unilever and LEK consulting. He holds an MBA from INSEAD and is an accredited executive coach
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-10" onclick="this.style.display = 'none';document.getElementById('more-10').style.display = 'block';document.getElementById('less-btn-10').style.display = 'block';">See more</a>
                        <a id="less-btn-10" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-10').style.display = 'none';document.getElementById('more-btn-10').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>

        <div id="speak-modal-11" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/09/christopher-persson.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Chris Persson</b>
                        <i class="speaker-position">RECAPEX</i>
                        <i class="speaker-position"><br>General Partner</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>Chris Persson is an entrepreneur and investor from Stockholm, Sweden. <br><br><span class="display-none" id="more-11">Entrepreneurial from a young age, he has started several companies including Vielife (sold to Cigna Healthcare) and Bookatable (sold to Michelin). Chris is a mentor at Seedcamp (London) and Singularity University Startup Labs (Silicon Valley), part of the .founders network (Dublin) and an advisor on early stage investments to some of Europe’s largest organisations within health and foodtech.</span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-11" onclick="this.style.display = 'none';document.getElementById('more-11').style.display = 'block';document.getElementById('less-btn-11').style.display = 'block';">See more</a>
                        <a id="less-btn-11" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-11').style.display = 'none';document.getElementById('more-btn-11').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-12" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10165538/christopher-moore.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Christopher Moore</b>
                        <i class="speaker-position">The Clink</i>
                        <i class="speaker-position"><br>Chief Executive</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>Christopher has more than 30 years’ experience in the hospitality industry having previously worked for Fenwick for 13 years as Head of Group Restaurants.
                    <br><br><span class="display-none" id="more-12">
                    Here he created and operated 37 restaurants across 12 UK sites. Prior to this, Chris worked at Harrods for eight years as Restaurants General Manager. Chris also worked for the Hilton Hotel Group for five years and trained with the Holiday Inn Hotel Group in New York. In 2010 Chris was appointed Chief Executive of The Clink Charity as part of a long-term plan to expand the prisoner training concept across Her Majesty’s Prison Service.
                    </span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-12" onclick="this.style.display = 'none';document.getElementById('more-12').style.display = 'block';document.getElementById('less-btn-12').style.display = 'block';">See more</a>
                        <a id="less-btn-12" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-12').style.display = 'none';document.getElementById('more-btn-12').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-13" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153144/Christopher-Slim.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Christopher Slim</b>
                        <i class="speaker-position">Nicoya Invest</i>
                        <i class="speaker-position"><br>Partner & Food Tech Investor</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>Christopher is a Partner and Food Tech investor at Nicoya Invest. <br><br><span class="display-none" id="more-13">Prior to this, he was a Partner and investor at Do Good Invest, a Swedish financial investment firm focusing on businesses with social impact as their main purpose. Before going into investment, Christopher accrued extensive experience in the FMCG industry, having held the positions of VP at Unilever Nordic and Business Lead at Procter & Gamble Nordic/Europe/Global. He was also the Global Head of Strategy at Absolut Vodka.</span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-13" onclick="this.style.display = 'none';document.getElementById('more-13').style.display = 'block';document.getElementById('less-btn-13').style.display = 'block';">See more</a>
                        <a id="less-btn-13" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-13').style.display = 'none';document.getElementById('more-btn-13').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-14" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/08/Daniel_Vennard-1.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Daniel Vennard</b>
                        <i class="speaker-position">World Resources Institute</i>
                        <i class="speaker-position"><br>Global Programme Director</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>Daniel is a Program Director at the World Resources Institute – the world's number one environmental think tank with over 500 staff based around the world.
                <br><br><span class="display-none" id="more-14">He runs a program called the Better Buying Lab that works with large food companies and experts in behaviour change and marketing to develop, test and scale new ideas that help shift consumers towards eating more sustainable diets. Prior to the World Resources Institute, Daniel worked for the last fifteen years at Mars Incorporated and Procter & Gamble in corporate strategy, sustainability and marketing. He has written and presented on how consumers can be shifted towards buying more sustainable products.</span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-14" onclick="this.style.display = 'none';document.getElementById('more-14').style.display = 'block';document.getElementById('less-btn-14').style.display = 'block';">See more</a>
                        <a id="less-btn-14" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-14').style.display = 'none';document.getElementById('more-btn-14').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-15" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/09/21152120/darren-goldsby.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Darren Goldsby</b>
                        <i class="speaker-position">The Jamie Oliver Group</i>
                        <i class="speaker-position"><br>Chief Digital Officer</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>Darren has been Chief Digital Officer at Jamie Oliver since March, where he is responsible for driving the use of digital technology to improve all areas of the Jamie Oliver business.
                    <br><br><span class="display-none" id="more-15">He was previously Chief Digital Officer at Hearst Magazines UK where he helped launch the Good Housekeeping Institute, and has worked in the media industry for over 15 years with previous roles at The Financial Times, News Corporation and Rightmove.</span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-15" onclick="this.style.display = 'none';document.getElementById('more-15').style.display = 'block';document.getElementById('less-btn-15').style.display = 'block';">See more</a>
                        <a id="less-btn-15" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-15').style.display = 'none';document.getElementById('more-btn-15').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-16" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153147/David-McIntyre-Airbnb.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">David McIntyre</b>
                        <i class="speaker-position">Airbnb</i>
                        <i class="speaker-position"><br>Global Head of Food</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>David McIntyre oversees all things Food for Airbnb, including self-operating dining programs for 35 global offices and business development for Airbnb's Experiences platform.
                    <br><br><span class="display-none" id="more-16">Before joining the tech sector, David’s career has included work in retail, culinary education, academic research, advocacy and policy.</span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-16" onclick="this.style.display = 'none';document.getElementById('more-16').style.display = 'block';document.getElementById('less-btn-16').style.display = 'block';">See more</a>
                        <a id="less-btn-16" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-16').style.display = 'none';document.getElementById('more-btn-16').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-17" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205821/denise-obsajsnik.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Denise Obsajsnik</b>
                        <i class="speaker-position">My Chef's Table</i>
                        <i class="speaker-position"><br>Founder</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>After spending over 15 years in financial services across Programme Management and Sales roles, Denise has ventured into the start up technology space.
                    <br><br><span class="display-none" id="more-17">She had created My Chef’s Table app which aims to create a visually engaging and enhanced dining experience and share the story of the chef and of their dishes including sourcing and approach to their creations.</span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-17" onclick="this.style.display = 'none';document.getElementById('more-17').style.display = 'block';document.getElementById('less-btn-17').style.display = 'block';">See more</a>
                        <a id="less-btn-17" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-17').style.display = 'none';document.getElementById('more-btn-17').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-18" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170233/diane-barlow.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Diane Barlow</b>
                        <i class="speaker-position">Food Standards Agency</i>
                        <i class="speaker-position"><br>Digital Service Manager</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>We are the Food Standards Agency, an independent Government department.
                    <br><br><span class="display-none" id="more-18">It’s our job to use our expertise and influence so that people can trust that the food they buy and eat is safe and honest. Our work touches everyone in the country. We all eat. Here you can find out more about the strategic approaches that underpin our work. Dianae is looking at ways the Food Standards Agency can work with Food Tech Business to help ensure that food in the UK is as safe as possible.</span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-18" onclick="this.style.display = 'none';document.getElementById('more-18').style.display = 'block';document.getElementById('less-btn-18').style.display = 'block';">See more</a>
                        <a id="less-btn-18" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-18').style.display = 'none';document.getElementById('more-btn-18').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-19" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153151/Dr.-Gary-Stutte.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Dr Gary Stutte</b>
                        <i class="speaker-position">SyNRGE</i>
                        <i class="speaker-position"><br>Scientist & Consultant</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>Dr. Gary Stutte led research at NASA’s John F. Kennedy Space Centre to develop sustainable food production systems for long duration space missions to the Moon, Mars and beyond.
                    <br><br><span class="display-none" id="more-19">Dr. Stutte is actively engaged in developing ground-based applications for space technology in biosciences, protected agriculture and commercial horticulture. He has published over 150 scientific articles on the effects of growth conditions on crops grown in closed environments, fruit production, space biology, LED lighting systems, and biological life support systems for missions to the Moon and Mars. Dr. Stutte is a founding member and Executive Director for the American Council for Medicinally Active Plants. He founded SyNRGE, LLC in 2015, specializing in Space Agriculture and Controlled Environment Technology. </span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-19" onclick="this.style.display = 'none';document.getElementById('more-19').style.display = 'block';document.getElementById('less-btn-19').style.display = 'block';">See more</a>
                        <a id="less-btn-19" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-19').style.display = 'none';document.getElementById('more-btn-19').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>
        <div id="speak-modal-20" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153150/Dr-Regan-Koch.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Dr Regan Koch</b>
                        <i class="speaker-position">Queen Mary University of London</i>
                        <i class="speaker-position"><br>Lecturer in Human Geography</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div">
                    <br>Regan Koch is a Lecturer in Human Geography at Queen Mary University of London.
                    <br><br><span class="display-none" id="more-20">His interests are in matters of urban public space, collective culture, and the representation and imagination of cities. Working between London and cities across the USA, his research has explored food-related trends, social entrepreneurship, regulation and changes in how we live together in cities. He is co-editor of Key Thinkers on Cities (Sage 2017) and is currently working on a monograph entitled The Public Life of Cities (both with Alan Latham, UCL). Regan has a PhD in Geography and Urban Studies from UCL.</span><br><br>
                    <div class="row float-right">
                        <a id="more-btn-20" onclick="this.style.display = 'none';document.getElementById('more-20').style.display = 'block';document.getElementById('less-btn-20').style.display = 'block';">See more</a>
                        <a id="less-btn-20" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-20').style.display = 'none';document.getElementById('more-btn-20').style.display = 'block';">See less</a><br><br>
                    </div>
                </div>
            </div>
        </div>

        <div id="speak-modal-21" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10092610/emma-woods.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Emma Woods</b>
                        <i class="speaker-position">wagamama</i>
                        <i class="speaker-position"><br>Customer Director</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Emma is the Customer Director of wagamama. She is passionate about the power of tech to improve both customer and team experiences in restaurants .
                    <br><br><span class="display-none" id="more-21">
She won Innovation of the year in 2011 for PizzaExpress on launching the world’s first app with PayPal you could pay your bill on. She also held the role of Group Marketing and Digital Director of Merlin plc (2013-2017) .
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-21" onclick="this.style.display = 'none';document.getElementById('more-21').style.display = 'block';document.getElementById('less-btn-21').style.display = 'block';">See more</a>
                        <a id="less-btn-21" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-21').style.display = 'none';document.getElementById('more-btn-21').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-22" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10093044/fraser-bradshaw.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Fraser Bradshaw</b>
                        <i class="speaker-position">IMBIBA</i>
                        <i class="speaker-position"><br>Partner</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Fraser is a Partner at venture capital firm Imbiba. Imbiba are specialist investors in the leisure and hospitality sector. Over the past 19 years they have achieved an average IRR of 35% from 10 exits, including Drake & Morgan.
                    <br><br><span class="display-none" id="more-22">
Fraser has over 25 years’ experience in commercial management and strategic marketing. He started his management career with ICI, before working in marketing and media, formerly as managing director at global advertising agency McCann Erickson, before his appointment as CEO of creative agency saintnicks. Fraser oversees deal origination, business appraisal, portfolio management, strategic marketing and fund marketing at Imbiba.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-22" onclick="this.style.display = 'none';document.getElementById('more-22').style.display = 'block';document.getElementById('less-btn-22').style.display = 'block';">See more</a>
                        <a id="less-btn-22" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-22').style.display = 'none';document.getElementById('more-btn-22').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-23" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205743/fredrika-gulfot.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Fredrika Gulfot</b>
                        <i class="speaker-position">Simris</i>
                        <i class="speaker-position"><br>Founder &amp; CEO</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Fredrika Gullfot is the CEO and founder of Simris Alg, a pioneering agribusiness listed on the Nasdaq First North growth market. Simris' main focus is the production of omega-3 from farmed algae as a premium alternative to fish oil.
                    <br><br><span class="display-none" id="more-23">
                    Fredrika holds a PhD in biotechnology from KTH Royal Institute of Technology in Stockholm. She is the first female awarded with the Swedish Chemical Engineering Prize and is a WIRED Innovation Fellow, appointed by Wired UK in 2015 as one of 12 innovators with the potential to change the world. Simris was recently awarded with the Swedish Food Award 2017 for its contributions to renewal of the industry.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-23" onclick="this.style.display = 'none';document.getElementById('more-23').style.display = 'block';document.getElementById('less-btn-23').style.display = 'block';">See more</a>
                        <a id="less-btn-23" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-23').style.display = 'none';document.getElementById('more-btn-23').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-24" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10093351/gerardo-mazzeo.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Gerardo Mazzeo</b>
                        <i class="speaker-position">Nestle</i>
                        <i class="speaker-position"><br>Global Innovation Director</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Gerardo has a wealth of experience in commercial and marketing roles at Nestlé and has been leading the company’s global innovation activities since 2012, finding new and innovative ways to solve challenges across the business. Since early 2016, he has also been leading the HENRi@Nestlé open innovation initiative.
                    <br><br><span class="display-none" id="more-24">
By connecting Nestlé businesses with the best and brightest innovators and start-ups, the platform helps Nestlé to forge partnerships that can tackle the projects that matter; enhancing quality of life and contributing to a healthier future for all.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-24" onclick="this.style.display = 'none';document.getElementById('more-24').style.display = 'block';document.getElementById('less-btn-24').style.display = 'block';">See more</a>
                        <a id="less-btn-24" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-24').style.display = 'none';document.getElementById('more-btn-24').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-25" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10093655/hannah-seal.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Hannah Seal</b>
                        <i class="speaker-position">Index Ventures</i>
                        <i class="speaker-position"><br>Investor</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    At Index, Hannah focuses on early stage investments across multiple sectors in the UK. She has a particular interest in Marketplaces, eCommerce, SaaS and Healthcare. Prior to joining Index, Hannah was responsible for the core Consumer to Consumer business at eBay UK.
                    <br><br><span class="display-none" id="more-25">
She has also worked at Ocado, where she helped set up the general merchandise business, and at Roland Berger, where she advised clients across Europe on key strategic topics within the retail, telecoms and transportation industries.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-25" onclick="this.style.display = 'none';document.getElementById('more-25').style.display = 'block';document.getElementById('less-btn-25').style.display = 'block';">See more</a>
                        <a id="less-btn-25" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-25').style.display = 'none';document.getElementById('more-btn-25').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-26" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10094323/hulya-erdal.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Hulya Erdal</b>
                        <i class="speaker-position">Made by the Chef</i>
                        <i class="speaker-position"><br>Chef</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Hulya works as a chef, educator, marketeer and culinary consultant, developing cookery courses and training others to be chefs.
                    <br><br><span class="display-none" id="more-26">
                    From charity organisations to private institutions, Hulya utilises over 20 years of food knowledge and experience within the catering and hospitality sector to benefit educational establishments looking to grow their brand and advance in the world of gastronomy.
                    </span>
                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-26" onclick="this.style.display = 'none';document.getElementById('more-26').style.display = 'block';document.getElementById('less-btn-26').style.display = 'block';">See more</a>
                        <a id="less-btn-26" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-26').style.display = 'none';document.getElementById('more-btn-26').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-27" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/09/06133537/Jamie-Bolding2-1.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Jamie Bolding</b>
                        <i class="speaker-position">Jungle Creations</i>
                        <i class="speaker-position"><br>Founder & CEO</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Jamie is the Founder and CEO of Jungle Creations, the third most viewed media property in the UK and the fifth most viewed in the world, which also owns VT, crowned the most viewed Facebook channel in the world in March 2017.
                    <br><br><span class="display-none" id="more-27">
                    Jamie created VT in 2014, which quickly gained over one million followers. From this 16 other Facebook channels were born that cover a whole breadth of topics, including 'Twisted', the company's food channel, and 'Bosh' its channel championing vegan recipes. Today Jungle Creations works with some of the world's biggest brands to create original and engaging content for its over 43 million followers.
                    </span>
                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-27" onclick="this.style.display = 'none';document.getElementById('more-27').style.display = 'block';document.getElementById('less-btn-27').style.display = 'block';">See more</a>
                        <a id="less-btn-27" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-27').style.display = 'none';document.getElementById('more-btn-27').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-28" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205744/jamie-spafford.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Jamie Spafford</b>
                        <i class="speaker-position">SORTEDfood</i>
                        <i class="speaker-position"><br>Co-Founder</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    SORTEDfood is one of the largest and most engaged food channels on YouTube with a global community of over 2 million fans.
                    <br><br><span class="display-none" id="more-28">
When he’s not on screen cooking (or mainly eating!), co-founder Jamie Spafford looks after community, media and partner relationships.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-28" onclick="this.style.display = 'none';document.getElementById('more-28').style.display = 'block';document.getElementById('less-btn-28').style.display = 'block';">See more</a>
                        <a id="less-btn-28" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-28').style.display = 'none';document.getElementById('more-btn-28').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-29" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10094841/jeremy-knight1.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Jeremy Knight</b>
                        <i class="speaker-position">Hej Coffee</i>
                        <i class="speaker-position"><br>Chief Espresso Officer</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    The origins of HEJ Coffee were conceived in Sweden and born in Bermondsey.
                    <br><br><span class="display-none" id="more-29">
Following a brief marriage to a large Swedish coffee conglomerate, Jeremy left the big corporate world to go back to what he loves most. After the divorce Jeremy kept the name, the café and strong Swedish values.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-29" onclick="this.style.display = 'none';document.getElementById('more-29').style.display = 'block';document.getElementById('less-btn-29').style.display = 'block';">See more</a>
                        <a id="less-btn-29" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-29').style.display = 'none';document.getElementById('more-btn-29').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-30" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10095121/joanna-de-koning.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Joanna De Koning</b>
                        <i class="speaker-position">Just Eat</i>
                        <i class="speaker-position"><br>Head of Corporate Communications</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Jo is head of corporate communications at Just Eat, a leading global marketplace for online food delivery. Just Eat operates in 12 markets and has 19 million customers and 74,500 restaurant partners globally.
                    <br><br><span class="display-none" id="more-30">
Jo is responsible for the company's external narrative and responsible business strategy, working closely with Just Eat's Executive Team, marketing and investor relations colleagues to ensure the company's voice is heard on the issues that matter. Prior to joining Just Eat, Jo led the European communications team at McDonald's, ensuring that McDonald's external messaging was aligned across 38 markets. She started her career at communications consultancy, Fishburn Hedges.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-30" onclick="this.style.display = 'none';document.getElementById('more-30').style.display = 'block';document.getElementById('less-btn-30').style.display = 'block';">See more</a>
                        <a id="less-btn-30" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-30').style.display = 'none';document.getElementById('more-btn-30').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>


        <div id="speak-modal-31" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205829/jonathan-petrides.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Jonathan Petrides (JP)</b>
                            <i class="speaker-position">allplants</i>
                            <i class="speaker-position"><br>Founder</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        JP is the founder of allplants, delivering hand-made plant-based deliciousness, fresh-frozen to your doorstep. They shamelessly use bold flavours and hyper convenience to bring more plants onto plates across the UK, inspiring us all to eat healthy and nourish the planet.
            <br><br><span class="display-none" id="more-31">
            He previously launched Penda Health, an award-winning Nairobi-based chain of retail Medical Centres, and prior to that started Africa's first mobile bank from paper concept to +10M previously unbanked users in Kenya borrowing and saving over SMS, M-Shwari is now a household name across East Africa.
            </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-31" onclick="this.style.display = 'none';document.getElementById('more-31').style.display = 'block';document.getElementById('less-btn-31').style.display = 'block';">See more</a>
                            <a id="less-btn-31" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-31').style.display = 'none';document.getElementById('more-btn-31').style.display = 'block';">See less</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
        <div id="speak-modal-32" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Kalinka Kay</b>
                            <i class="speaker-position">TrueStart</i>
                            <i class="speaker-position"><br></i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
            <br><br><span class="display-none" id="more-32">
            </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-32" onclick="this.style.display = 'none';document.getElementById('more-32').style.display = 'block';document.getElementById('less-btn-32').style.display = 'block';">See more</a>
                            <a id="less-btn-32" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-32').style.display = 'none';document.getElementById('more-btn-32').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-33" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://yfood.com/wp-content/uploads/2016/08/Kevin-Duffy_Winnow-1.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Kevin Duffy</b>
                            <i class="speaker-position">Winnow</i>
                            <i class="speaker-position"><br>Co-Founder</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Kevin co-founded Winnow to help the hospitality sector cut food waste using technology. Winnow is a smart meter to help chefs measure, monitor and dramatically reduce food waste.
            <br><br><span class="display-none" id="more-33">
            Since launching in 2013, Winnow has been deployed in over 600 kitchens in 30 countries.
            </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-33" onclick="this.style.display = 'none';document.getElementById('more-33').style.display = 'block';document.getElementById('less-btn-33').style.display = 'block';">See more</a>
                            <a id="less-btn-33" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-33').style.display = 'none';document.getElementById('more-btn-33').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-34" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06225520/kimberly-hurd.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Kimberly Hurd</b>
                            <i class="speaker-position">Tabl</i>
                            <i class="speaker-position"><br>Co-Founder &amp; CEO</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Kimberly is the CEO and Co-Founder of Tabl, the UK’s first online marketplace blending independent food and events that serves as a home for its ever-growing food community. Prior to Tabl, Kimberly headed up Zomato UK - the restaurant search and discovery site and app.
            <br><br><span class="display-none" id="more-34">
            Before taking the leap into her passion for food, Kimberly spent 10 years in growth strategy and product development in Asia, Europe, Mexico, and the US, for companies including UBS, Moody’s, Accenture and the World Gold Council. She holds an MBA from the London Business School and while not spinning plates at Tabl and The Food Corporation, she’s chasing after her dog and husband or experimenting in the kitchen.
            </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-34" onclick="this.style.display = 'none';document.getElementById('more-34').style.display = 'block';document.getElementById('less-btn-34').style.display = 'block';">See more</a>
                            <a id="less-btn-34" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-34').style.display = 'none';document.getElementById('more-btn-34').style.display = 'block';">See less</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
        <div id="speak-modal-35" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170535/kirsty-macdonald.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Kirsty Macdonald</b>
                            <i class="speaker-position">Jam Jar Investments</i>
                            <i class="speaker-position"><br>Senior Investment Associate</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Kirsty is a Senior Investment Associate at JamJar. She was previously at Unilever doing marketing and customer analytics for a host of well-respected brands.
                <br><br><span class="display-none" id="more-35">
                She graduated from Exeter College, Oxford with a degree in Economics & Management and a Masters from LSE in Politics.
                </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-35" onclick="this.style.display = 'none';document.getElementById('more-35').style.display = 'block';document.getElementById('less-btn-35').style.display = 'block';">See more</a>
                            <a id="less-btn-35" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-35').style.display = 'none';document.getElementById('more-btn-35').style.display = 'block';">See less</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
        <div id="speak-modal-36" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170536/kishan-vasani.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Kishan Vasani</b>
                            <i class="speaker-position">Dishq</i>
                            <i class="speaker-position"><br>Co-Founder &amp; CEO</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Kishan is the Co-Founder & CEO. He’s been working in startups for 10 years including 3.5 years at Just Eat Group, the world's largest online food ordering platform.
                    <br><br><span class="display-none" id="more-36">
                    First, as the Head of International Marketing, he worked on digital acquisition and brand across 8 markets. Then he moved to India to take charge of Just Eat's Indian business as COO where he was responsible for 100 people across all functions. He oversaw 300% growth, making the company the Indian market leader in 12 months. Prior to his time at Just Eat, Kishan was the Founder of a digital marketing business in London for 5 years. During that time he also co-founded a UK based education charity, One Cause, that supports grassroots educational organisations in India and Uganda.
                    </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-36" onclick="this.style.display = 'none';document.getElementById('more-36').style.display = 'block';document.getElementById('less-btn-36').style.display = 'block';">See more</a>
                            <a id="less-btn-36" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-36').style.display = 'none';document.getElementById('more-btn-36').style.display = 'block';">See less</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
        <div id="speak-modal-37" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153153/Leonie-Cooper.png" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Leonie Cooper</b>
                            <i class="speaker-position">London City Hall</i>
                            <i class="speaker-position"><br>Chair of the Environment Committee</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Leonie Cooper AM represents Merton & Wandsworth at the London Assembly. Assembly Members scrutinise the Mayor and bodies of the GLA to protect the interests of and enhance the lives of Londoners.
                <br><br><span class="display-none" id="more-37">
                Leonie is Chair of the Environment Committee, which examines all aspects of the capital’s environment by reviewing the Mayor’s strategies on air quality, water, waste, climate change and energy. The committee looks at what additional measures could be taken to help improve the quality of life for Londoners and investigates topics ranging from airport expansion and burst water mains, to noise from the Night Tube and the environmental impacts of plastic water bottles.
                </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-37" onclick="this.style.display = 'none';document.getElementById('more-37').style.display = 'block';document.getElementById('less-btn-37').style.display = 'block';">See more</a>
                            <a id="less-btn-37" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-37').style.display = 'none';document.getElementById('more-btn-37').style.display = 'block';">See less</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
        <div id="speak-modal-38" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/09/21152121/libby-andrews.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Libby Andrews</b>
                            <i class="speaker-position">Pho</i>
                            <i class="speaker-position"><br>Head of Marketing</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Libby has extensive marketing experience in the United States and the UK in the food and drink industry.
                    <br><br><span class="display-none" id="more-38">
                    She has coordinated national multi-channel campaigns for restaurants, retail brands and culinary personalities including Chilango, Hearst Magazines, Knopf Doubleday Publishing Group, the Julia Child Foundation, GLORIOUS! Soups, Sweet Chick and Action Against Hunger. She is also one of the founding members of Ladies of Restaurants, a networking organisation for women who work in hospitality.
                    </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-38" onclick="this.style.display = 'none';document.getElementById('more-38').style.display = 'block';document.getElementById('less-btn-38').style.display = 'block';">See more</a>
                            <a id="less-btn-38" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-38').style.display = 'none';document.getElementById('more-btn-38').style.display = 'block';">See less</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
        <div id="speak-modal-39" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170825/lisa-jenkins.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Lisa Jenkins</b>
                            <i class="speaker-position">Caterer</i>
                            <i class="speaker-position"><br>Product Editor</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Lisa Jenkins is the products and suppliers editor of The Caterer.  Her remit includes writing about all products; food, drink and equipment and all the companies who manufacture, supply and distribute them.
                        <br><br><span class="display-none" id="more-39">
                        She has been involved in hospitality for most of her life.  Her parents managed pubs – and she herself has worked in hospitality; behind the bar, in the kitchen and front of house. She has been part of the Caterer team, on and off for nearly 14 years, and served her time in hotels with a four year sojourn as group events manager at Malmaison and Hotel du Vin hotels including six hotel openings.
                        </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-39" onclick="this.style.display = 'none';document.getElementById('more-39').style.display = 'block';document.getElementById('less-btn-39').style.display = 'block';">See more</a>
                            <a id="less-btn-39" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-39').style.display = 'none';document.getElementById('more-btn-39').style.display = 'block';">See less</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
        <div id="speak-modal-40" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10170942/mark-palmer.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Mark Palmer</b>
                            <i class="speaker-position">BT</i>
                            <i class="speaker-position"><br></i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                <br><br><span class="display-none" id="more-40">
                </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-40" onclick="this.style.display = 'none';document.getElementById('more-40').style.display = 'block';document.getElementById('less-btn-40').style.display = 'block';">See more</a>
                            <a id="less-btn-40" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-40').style.display = 'none';document.getElementById('more-btn-40').style.display = 'block';">See less</a><br><br>
                        </div>
                    </div>
                </div>
            </div>
        <div id="speak-modal-41" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://yfood.com/wp-content/uploads/2016/09/Michael-Atkinson.png" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Michael Atkinson</b>
                            <i class="speaker-position">Bailiwick Ventures</i>
                            <i class="speaker-position"><br>CEO</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Michael is a restaurant technology investor, entrepreneur and strategic advisor to brands, venture investors, investment banks and technology innovators worldwide.
                <br><br><span class="display-none" id="more-41">
Michael founded FohBoh.com, inFOH.com and Quikfitapp.com. He is a non-executive Director at Metristo.com; founder and CEO of Orderscape.com; advisor to bluestartups.com, foodandcity.org and 6dbytes.com, a food-maker robotics company. Most recently, Michael founded Roostersbot a fully autonomous food-maker robot being developed for release in late 2018. As an advocate for food and restaurant technologies, Michael helps restaurant brands and early-stage food and restaurant technology companies in the areas of corporate development, corporate finance, strategy, product development and resource integration.

                </span>

                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-41" onclick="this.style.display = 'none';document.getElementById('more-41').style.display = 'block';document.getElementById('less-btn-41').style.display = 'block';">See more</a>
                            <a id="less-btn-41" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-41').style.display = 'none';document.getElementById('more-btn-41').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-42" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10090505/nick-popovici.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Nick Popovici</b>
                            <i class="speaker-position">VitaMojo</i>
                            <i class="speaker-position"><br>Co-Founder</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Nick is passionate about transforming the restaurant industry through innovative technology.
                <br><br><span class="display-none" id="more-42">
Before Vita Mojo he spent 8 years in finance as a Portfolio Manager for BlackRock and most recently for Schroders.

                </span>

                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-42" onclick="this.style.display = 'none';document.getElementById('more-42').style.display = 'block';document.getElementById('less-btn-42').style.display = 'block';">See more</a>
                            <a id="less-btn-42" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-42').style.display = 'none';document.getElementById('more-btn-42').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-43" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Oksana Stowe</b>
                            <i class="speaker-position">TrueStart</i>
                            <i class="speaker-position"><br></i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>

                <br><br><span class="display-none" id="more-43">
                </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-43" onclick="this.style.display = 'none';document.getElementById('more-43').style.display = 'block';document.getElementById('less-btn-43').style.display = 'block';">See more</a>
                            <a id="less-btn-43" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-43').style.display = 'none';document.getElementById('more-btn-43').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-44" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10172621/os-tariq.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Os Tariq</b>
                            <i class="speaker-position">Food Standards Agency</i>
                            <i class="speaker-position"><br></i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                    We are the Food Standards Agency, an independent Government department. It’s our job to use our expertise and influence so that people can trust that the food they buy and eat is safe and honest.
                        <br><br><span class="display-none" id="more-44">
                        Our work touches everyone in the country. We all eat. Here you can find out more about the strategic approaches that underpin our work. Os is looking at ways the Food Standards Agency can work with Food Tech Business to help ensure that food in the UK is as safe as possible.    
                        </span>
                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-44" onclick="this.style.display = 'none';document.getElementById('more-44').style.display = 'block';document.getElementById('less-btn-44').style.display = 'block';">See more</a>
                            <a id="less-btn-44" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-44').style.display = 'none';document.getElementById('more-btn-44').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-45" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/02153156/Paul-Newnham.png" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Paul Newnham</b>
                            <i class="speaker-position">SDG2 Advocacy Hub</i>
                            <i class="speaker-position"><br>Coordinator</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Paul Newnham currently heads up the SDG 2 Advocacy hub – coordinating global campaigning and advocacy to achieve Sustainable Development Goal (SDG) 2: To end hunger, achieve food security and improved nutrition, and promote sustainable agriculture by 2030 based out of the UN World Food Programme.
                <br><br><span class="display-none" id="more-45">
He has over 20 years’ experience in National and Global roles with a focus on Campaigning, Youth Mobilisation, Advocacy, Marketing and Communication. Paul is passionate about helping bring in new voices to the Global goals conversation particularly around Zero Hunger.

                </span>

                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-45" onclick="this.style.display = 'none';document.getElementById('more-45').style.display = 'block';document.getElementById('less-btn-45').style.display = 'block';">See more</a>
                            <a id="less-btn-45" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-45').style.display = 'none';document.getElementById('more-btn-45').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-46" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/09/21152122/peter-martin.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Peter Martin</b>
                            <i class="speaker-position">CGA</i>
                            <i class="speaker-position"><br>Vice President</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Peter Martin is the leading commentator on the UK eating and drinking-out market. He is vice president of CGA, the specialist data, insight and networking business, formed in 2013 when his Peach Factory company merged with CGA Strategy.
                <br><br><span class="display-none" id="more-46">
Peter has over 30 years experience in the sector as journalist, editor and media-owner and has launched and run a raft of industry-leading magazines, journals and events, including M&C Report and the Retailers Retailer Awards.

                </span>

                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-46" onclick="this.style.display = 'none';document.getElementById('more-46').style.display = 'block';document.getElementById('less-btn-46').style.display = 'block';">See more</a>
                            <a id="less-btn-46" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-46').style.display = 'none';document.getElementById('more-btn-46').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-47" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10051349/philip-james.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Philip James</b>
                            <i class="speaker-position">Sheridans</i>
                            <i class="speaker-position"><br>Partner</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Philip leads Sheridans’ Technology and Data Group and advises clients on commercial data strategy, brand integrity and IP asset growth.
                <br><br><span class="display-none" id="more-47">
He speaks regularly on the opportunities, challenges and trends posed by use of data assets, including big and open data, AI, behavioural targeting and cyber risk. He has acted as in-house counsel at Dell, Sparrowhawk International (now NBC Universal), Telewest/Flextech TV (now Virgin Media) and was acting (non-contentious) Head of Legal for Nokia UK.

                </span>

                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-47" onclick="this.style.display = 'none';document.getElementById('more-47').style.display = 'block';document.getElementById('less-btn-47').style.display = 'block';">See more</a>
                            <a id="less-btn-47" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-47').style.display = 'none';document.getElementById('more-btn-47').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-48" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/16173127/Charles-Spence.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Professor Charles Spence</b>
                            <i class="speaker-position">Oxford University</i>
                            <i class="speaker-position"><br>Head of the Crossmodal Research Laboratory at the Department of
                                Experimental Psychology</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Professor Charles Spence is a world-renowned experimental psychologist specialising in neuroscience-inspired multi-sensory design. He established the Crossmodal Research Laboratory (CRL) at the Department of Experimental Psychology, Oxford University in 1997.
                <br><br><span class="display-none" id="more-48">
Prof. Spence has published over 775 articles and edited or authored, 10 academic volumes including, in 2014, the Prose prize-winning “The Perfect Meal” and 2017's “Gastrophysics: The New Science of Eating”. Much of Prof. Spence’s work focuses on the design of enhanced multi-sensory food and drink experiences, through collaborations with chefs, baristas, mixologists, perfumiers, and the food and beverage, and flavour and fragrance industries. Prof. Spence has also worked extensively on the question of how technology will transform our dining experiences in the future.
                </span>

                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-48" onclick="this.style.display = 'none';document.getElementById('more-48').style.display = 'block';document.getElementById('less-btn-48').style.display = 'block';">See more</a>
                            <a id="less-btn-48" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-48').style.display = 'none';document.getElementById('more-btn-48').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-49" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10091749/robin-rowland.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Robin Rowland</b>
                            <i class="speaker-position">Yo! Sushi</i>
                            <i class="speaker-position"><br>CEO</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Robin became YO! Sushi’s CEO in 2000. Over last 15 years Robin and team have built the iconic restaurant chain from 3 to 80 company owned restaurants in the UK and USA plus over a dozen franchise restaurants in international airports and the Gulf region.
                <br><br><span class="display-none" id="more-49">
His management philosophy is simple as he agitates for ‘continual improvement’ on 5 Ps  - People, Product, Property, Promotion and Profit. He has been recognised by Retailer of Year, Catey, Peach Icon awards and the YO! Sushi team has been recognised for design, company, menu, technology and as an outstanding employer. Robin is also a member of ALMR board and is NED with Marstons PLC, Ethos (Gulf restaurant platform) and Caffe Nero.

                </span>

                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-49" onclick="this.style.display = 'none';document.getElementById('more-49').style.display = 'block';document.getElementById('less-btn-49').style.display = 'block';">See more</a>
                            <a id="less-btn-49" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-49').style.display = 'none';document.getElementById('more-btn-49').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>
        <div id="speak-modal-50" class="modalz">
                <div>
                    <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                    <div class="row">
                        <div class="large-3 columns">
                            <div class="speaker-image">
                                <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                            </div>
                        </div>
                        <div style="margin-top: 31px">
                            <b class="speaker-name">Saasha Celestial-One</b>
                            <i class="speaker-position">Olio</i>
                            <i class="speaker-position"><br>Co-Founder</i>
                        </div>
                    </div>
                    <hr>
                    <div class="modal-content-div"><br>
                        Saasha Celestial-One is the daughter of Iowa hippy entrepreneurs who was raised with a deep appreciation for the planet and its scarce resources. After completing her MBA at Stanford Graduate School of Business, where she met her OLIO co-founder Tessa, Saasha spent 9 years in various strategy and businesses development roles.
                <br><br><span class="display-none" id="more-50">
When on her maternity leave in 2012, she opened her first business in London, which she outfitted entirely from FreeCycle. It was a short skip and a hop to launching the food sharing app OLIO with Tessa in summer 2015. OLIO now has over a quarter of a million users globally and has facilitated over 80,000 neighbour-to-neighbour doorstep exchanges.

                </span>

                        <br><br>
                        <div class="row float-right">
                            <a id="more-btn-50" onclick="this.style.display = 'none';document.getElementById('more-50').style.display = 'block';document.getElementById('less-btn-50').style.display = 'block';">See more</a>
                            <a id="less-btn-50" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-50').style.display = 'none';document.getElementById('more-btn-50').style.display = 'block';">See less</a><br><br>
                        </div>

                    </div>
                </div>
            </div>


        <div id="speak-modal-51" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10171402/sai-sreenivas-kodur.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Sai Sreenivas Kodur</b>
                        <i class="speaker-position">The Sustainable Restaurant Association</i>
                        <i class="speaker-position"><br>Co-Founder &amp; CTO</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Sai is the Co-Founder &amp; CTO. A computer science graduate from IIT (Madras). He has more than 4 years of experience in solving the problems of search and discovery.

                    <br><br><span class="display-none" id="more-51">

He worked at Zomato, India's largest food ordering and restaurant discovery platform, as a Software Engineer in search, and re-built its search relevance &amp; ranking models to improve restaurant results. Later he worked at Myntra, India's biggest online fashion store, as Senior Software Engineer, leading search and style services, working on various aspects, including scaling up its search infrastructure for high revenue days, developing personalised filters based on users fashion preferences, and also conceptualising a generic search platform.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-51" onclick="this.style.display = 'none';document.getElementById('more-51').style.display = 'block';document.getElementById('less-btn-51').style.display = 'block';">See more</a>
                        <a id="less-btn-51" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-51').style.display = 'none';document.getElementById('more-btn-51').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-52" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/09/samad-masood.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Samad Masood</b>
                        <i class="speaker-position">House of Genius</i>
                        <i class="speaker-position"><br>London Lead Moderator</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Having started his career as a technology journalist just as the first dotcom boom was busting, Samad developed an early cynicism for the hype that surrounds new internet innovations.
                    <br><br><span class="display-none" id="more-52">
                    Since then he's led industry strategy and research programmes analysing the business of technology, and set up and run two successful London-based startup accelerators while leading Accenture's UK Open Innovation programme. But his heart is in House of Genius - where he can really let his cynical hype radar run riot.
                    </span>
                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-52" onclick="this.style.display = 'none';document.getElementById('more-52').style.display = 'block';document.getElementById('less-btn-52').style.display = 'block';">See more</a>
                        <a id="less-btn-52" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-52').style.display = 'none';document.getElementById('more-btn-52').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-53" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10171708/steven-novick.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Steven Novick</b>
                        <i class="speaker-position">Farmstand</i>
                        <i class="speaker-position"><br>Founder</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Since 2016, Andrew has been the CEO of the Sustainable Restaurant Association (SRA) a not-for-profit organisation focused solely on the challenge of fostering a better food system by directing, nudging, shaping, creating and measuring sustainable change in the food service sector.
                    <br><br><span class="display-none" id="more-53">
They do this through Food Made Good, a collaborative community of chefs, restaurateurs, marketers, foodies and social changers. This community exists on and offline to connect people working together to make food good for people and the planet. Andrew's mission to help us use the power of our appetites wisely.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-53" onclick="this.style.display = 'none';document.getElementById('more-53').style.display = 'block';document.getElementById('less-btn-53').style.display = 'block';">See more</a>
                        <a id="less-btn-53" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-53').style.display = 'none';document.getElementById('more-btn-53').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-54" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10172117/sue-cambage-hughes.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Sue Cambage-Hughes</b>
                        <i class="speaker-position"><br>CGA</i>
                        <i class="speaker-position"><br>Head of Consumer Research</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Sue Cambage-Hughes is CGA’s Head of Consumer Research. She joined in mid-2017, bringing a wealth of insight experience drawn from a career spanning banking and finance, corporate, retail and the youth market.
                    <br><br><span class="display-none" id="more-54">
She was formerly consumer insight lead for food at the Co-operative Group and most recently head of insight at the National Union of Students (NUS).

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-54" onclick="this.style.display = 'none';document.getElementById('more-54').style.display = 'block';document.getElementById('less-btn-54').style.display = 'block';">See more</a>
                        <a id="less-btn-54" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-54').style.display = 'none';document.getElementById('more-btn-54').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-55" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Tomoya Yasuda</b>
                        <i class="speaker-position"><br>Cookpad</i>
                        <i class="speaker-position"><br>&nbsp;</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Since 2016, Andrew has been the CEO of the Sustainable Restaurant Association (SRA) a not-for-profit organisation focused solely on the challenge of fostering a better food system by directing, nudging, shaping, creating and measuring sustainable change in the food service sector.
                    <br><br><span class="display-none" id="more-55">
They do this through Food Made Good, a collaborative community of chefs, restaurateurs, marketers, foodies and social changers. This community exists on and offline to connect people working together to make food good for people and the planet. Andrew's mission to help us use the power of our appetites wisely.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-55" onclick="this.style.display = 'none';document.getElementById('more-55').style.display = 'block';document.getElementById('less-btn-55').style.display = 'block';">See more</a>
                        <a id="less-btn-55" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-55').style.display = 'none';document.getElementById('more-btn-55').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-56" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://yfood.com/wp-content/uploads/2016/10/Virginie-Rabant.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Virginie Rabant</b>
                        <i class="speaker-position"><br>Elior</i>
                        <i class="speaker-position"><br>&nbsp;</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Since 2016, Andrew has been the CEO of the Sustainable Restaurant Association (SRA) a not-for-profit organisation focused solely on the challenge of fostering a better food system by directing, nudging, shaping, creating and measuring sustainable change in the food service sector.
                    <br><br><span class="display-none" id="more-56">
They do this through Food Made Good, a collaborative community of chefs, restaurateurs, marketers, foodies and social changers. This community exists on and offline to connect people working together to make food good for people and the planet. Andrew's mission to help us use the power of our appetites wisely.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-56" onclick="this.style.display = 'none';document.getElementById('more-56').style.display = 'block';document.getElementById('less-btn-56').style.display = 'block';">See more</a>
                        <a id="less-btn-56" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-56').style.display = 'none';document.getElementById('more-btn-56').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-57" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/08/16173129/Zoe-Chambers.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Zoe Chambers</b>
                        <i class="speaker-position"><br>Octopus Ventures</i>
                        <i class="speaker-position"><br>Investor</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Zoe joined the Octopus Ventures team in 2017 and she spends her time assessing and executing investment opportunities and assisting portfolio companies. One of her key focuses is food and ag-tech. Prior to joining Octopus, she spent c.8 years as a corporate lawyer with a US law firm in the City focused on M&amp;A, private equity and venture capital transactions. She also worked for Goldman Sachs, within their European Special Situations Group. Zoe holds a degree in Law with French Law from University College London.
                    <br><br><span class="display-none" id="more-57">
Prior to joining Octopus, she spent c.8 years as a corporate lawyer with a US law firm in the City focused on M&amp;A, private equity and venture capital transactions. She also worked for Goldman Sachs, within their European Special Situations Group. Zoe holds a degree in Law with French Law from University College London.
                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-57" onclick="this.style.display = 'none';document.getElementById('more-57').style.display = 'block';document.getElementById('less-btn-57').style.display = 'block';">See more</a>
                        <a id="less-btn-57" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-57').style.display = 'none';document.getElementById('more-btn-57').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-58" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/06205831/saasha-celestial-one.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Mattia-WendaSrl</b>
                        <i class="speaker-position"><br>Wenda</i>
                        <i class="speaker-position"><br>&nbsp;</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    We are an Italian innovative startup, delivering IoT solutions which support product quality and performance. Our goal is to turn data gathered into meaningful insights, enabling data driven decisions.
                    <br><br><span class="display-none" id="more-58">
WENDA's core business is actually in the Food&amp;Beverage market areas.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-58" onclick="this.style.display = 'none';document.getElementById('more-58').style.display = 'block';document.getElementById('less-btn-58').style.display = 'block';">See more</a>
                        <a id="less-btn-58" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-58').style.display = 'none';document.getElementById('more-btn-58').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-59" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10174328/oliver-oram.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Oliver Oram</b>
                        <i class="speaker-position"><br>CEO</i>
                        <i class="speaker-position"><br>Chainvine</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Since 2016, Andrew has been the CEO of the Sustainable Restaurant Association (SRA) a not-for-profit organisation focused solely on the challenge of fostering a better food system by directing, nudging, shaping, creating and measuring sustainable change in the food service sector.
                    <br><br><span class="display-none" id="more-59">
They do this through Food Made Good, a collaborative community of chefs, restaurateurs, marketers, foodies and social changers. This community exists on and offline to connect people working together to make food good for people and the planet. Andrew's mission to help us use the power of our appetites wisely.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-59" onclick="this.style.display = 'none';document.getElementById('more-59').style.display = 'block';document.getElementById('less-btn-59').style.display = 'block';">See more</a>
                        <a id="less-btn-59" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-59').style.display = 'none';document.getElementById('more-btn-59').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-60" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/03/02045531/Chris-Fung.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Chris Fung</b>
                        <i class="speaker-position"><br>Crussh Fit Food &amp; Juice Bars</i>
                        <i class="speaker-position"><br>CEO</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Chris is the former CEO of Crussh Fit Food &amp; Juice Bars, the UK’s leading juice, smoothie and healthy eating chain with over 30 sites in London.
                    <br><br><span class="display-none" id="more-60">
                    Having led Crussh for 13 years, he is now a non-exec board advisor investing in and guiding a number of businesses with a specific focus on the Food Tech space.
                    </span>
                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-60" onclick="this.style.display = 'none';document.getElementById('more-60').style.display = 'block';document.getElementById('less-btn-60').style.display = 'block';">See more</a>
                        <a id="less-btn-60" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-60').style.display = 'none';document.getElementById('more-btn-60').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-61" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10175258/trewin-restorick.jpg" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Trewin Restorick</b>
                        <i class="speaker-position"><br>Hubbub</i>
                        <i class="speaker-position"><br>CEO</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Previously Trewin created Global Action Plan, the UK’s leading environmental behaviour change charity which he set up in 1993.  As CEO Trewin developed award winning sustainability partnerships with organisations such as BskyB, Barts NHS Trust and EON as well as developing a wide range of successful environmental initiatives in schools and communities.
                    <br><br><span class="display-none" id="more-61">
Trewin is a frequent commentator on environmental issues including regular slots in Business Green, Guardian Sustainable Business and Sky News.  He has been trained by Al Gore as one of his climate change ambassadors. Before starting Global Action Plan, Trewin was Director of Fundraising at Friends of the Earth where he created PaperRound, London’s leading community recycling business.  Prior to Friends of the Earth, Trewin worked in the job creation team at Plymouth City Council and also produced a youth TV programme for the BBC called Something Else.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-61" onclick="this.style.display = 'none';document.getElementById('more-61').style.display = 'block';document.getElementById('less-btn-61').style.display = 'block';">See more</a>
                        <a id="less-btn-61" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-61').style.display = 'none';document.getElementById('more-btn-61').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>
        <div id="speak-modal-62" class="modalz">
            <div>
                <b><a href="#modal-close" title="Close" class="modal-close">X</a></b>

                <div class="row">
                    <div class="large-3 columns">
                        <div class="speaker-image">
                            <img src="https://s3-eu-west-1.amazonaws.com/yfood-web-storage/wp-content/uploads/2017/10/10171708/steven-novick.png" alt="speaker_image">
                        </div>
                    </div>
                    <div style="margin-top: 31px">
                        <b class="speaker-name">Steven Novick</b>
                        <i class="speaker-position"><br>Farmstand</i>
                        <i class="speaker-position"><br>Founder</i>
                    </div>
                </div>
                <hr>
                <div class="modal-content-div"><br>
                    Steven grew up in a working class family in Milwaukee, Wisconsin across the street from a farm. In 2006, he was diagnosed with cancer and he immediately wanted to change the way he ate yet finding honestly healthy food was nearly impossible.
                    <br><br><span class="display-none" id="more-62">
So before he got the all-clear, Steven set three goals: become closer to friends and family, finish climbing the Seven Summits and build a healthy, sustainable food company. Farmstand, described as Whole Foods and Patagonia meets Amazon Prime, is a healthy, sustainable fast-casual food tech company founded in London in January 2016. They offer breakfast, lunch, dinner, snacks and drinks. Farmstand targets businesses rather than individuals. In 20 months, they manage 20 Farmstands and sell their products via 3 distribution channels: their flagship restaurant in Covent Garden, at corporates and online.

                    </span>

                    <br><br>
                    <div class="row float-right">
                        <a id="more-btn-62" onclick="this.style.display = 'none';document.getElementById('more-62').style.display = 'block';document.getElementById('less-btn-62').style.display = 'block';">See more</a>
                        <a id="less-btn-62" class="display-none" onclick="this.style.display = 'none';document.getElementById('more-62').style.display = 'none';document.getElementById('more-btn-62').style.display = 'block';">See less</a><br><br>
                    </div>

                </div>
            </div>
        </div>

        <?php do_action( 'foundationpress_after_content' ); ?>
    </div>

    <input type="hidden" id="absolute_url" value="<?=dirname(__FILE__)?>/data.json">

    <style>
        .modalz {
            position: fixed;
            background-color: rgba(0, 0, 0, 0.15);
            top: 0;
            overflow-y: scroll;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 999;
            opacity: 0;
            pointer-events: none;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }

        .modalz:target {
            opacity: 1;
            pointer-events: auto;
        }

        .modalz>div {
            width: 70%;
            position: relative;
            margin: 10% auto;
            padding: 2rem;
            background: #fff;
            color: #444;
        }

        .modalz header {
            font-weight: bold;
        }

        .modal-close {
            color: #aaa;
            line-height: 50px;
            font-size: 80%;
            position: absolute;
            right: 0;
            text-align: center;
            top: 0;
            width: 70px;
            text-decoration: none;
        }

        .modal-close:hover {
            color: #000;
        }

        .modalz h1 {
            font-size: 150%;
            margin: 0 0 15px;
        }
        .modal-content-div
        {
            margin-right: 21px;
            margin-bottom: 21px;
        }
        .display-none
        {
            display: none;
        }
        .speaker-position {
            color:#808080;
        }
        .speaker-name {
            display: table-row;
            margin-top: 11px;
        }
    </style>

<?php get_footer();
