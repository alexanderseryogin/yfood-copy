<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'yfooddatadb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
//define('DB_PASSWORD', 'QfYZwOfG(7JI');
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*B1 D6/h<G8ICv=hd!XGI->)a/TJHv<VIepq[Jht%*c-MwuX_8$h*n).>tE}}K+h');
define('SECURE_AUTH_KEY',  '6R%d~OEDZh16_B 8v/P#HA &ZDi0HE|7f(c`jwQa,``>YH}Q}%-z>(L6v0ZYonaj');
define('LOGGED_IN_KEY',    '?QQp->fB]!4fg6(_>A!dr]=T;$DN74yJj <QGI.s4^?*.wk8:O/vVl9S_5Lh,S!&');
define('NONCE_KEY',        'lq% M!z1YL:U567r(0y*Gzk>_v)rAB<(+F,m~#kH=9c[p].*2P6=~D|x.ybNlqyj');
define('AUTH_SALT',        'j9?XMrt!63f;Ne=YYqw$>n7&CJ9OKu Tf>1-CAn8<*q20sOZF~ctw*BMz1X!TqLX');
define('SECURE_AUTH_SALT', 'T8AC<lmU.q,$g6M+c=zRL<(jn?Z%%O}~AO)LY!@5y(R9;Va:,UJ|#S,82)<0 {,?');
define('LOGGED_IN_SALT',   'Dfm$tfnlM+4{ak&R@U?N1uJL+=-;U|Qp!y9,Uc%9tQ/vVZHIu;3`)R)*3o`0;F?9');
define('NONCE_SALT',       'w+6UJ$1X*{Zjfg^=IN}4VZ;q1!Gu_5/tvt5H@^Xu?v/Z0@(g}wcux?QcAaG|%_w1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
/* ini_set('display_errors','Off'); */
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);

/* ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true); */


define('WP_ENV', 'staging');



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
